use tinycmplx::*;
use crate::iir::BiquadCascade;
use crate::util::clamp;

/// Those were chosen empirically.
const FILTER_ORDER: usize = 10;
const SYMBOL_FILTER_ORDER: usize = 3;
const SYMBOL_BW_FACTOR: f32 = 3f32;

pub struct FSKDemod {
    sample_rate: f32,
    center_frequency: f32,
    shift: f32,
    filter: BiquadCascade<Complex32>,
    symbol_filter: BiquadCascade<f32>,
    lo_phase: f32,
    prev_out: Complex32,
}


/// A FSK demodulator implementation.
/// Expects input samples in the range -1 ... 1.
impl FSKDemod {
    /// Initializes the structure.
    pub fn new(sample_rate: f32, center_frequency: f32, baud_rate: f32, shift: f32) -> Self {
        let cutoff = (baud_rate + shift * 2f32) / sample_rate / 2f32;
        assert!(cutoff < 0.5f32);

        let symbol_cutoff = baud_rate / sample_rate * SYMBOL_BW_FACTOR;
        assert!(symbol_cutoff < 0.5f32);

        FSKDemod {
            sample_rate,
            center_frequency,
            shift,
            filter: BiquadCascade::butterworth_lowpass(FILTER_ORDER, cutoff),
            symbol_filter: BiquadCascade::butterworth_lowpass(SYMBOL_FILTER_ORDER, symbol_cutoff),
            lo_phase: 0.0f32,
            prev_out: Complex32::ZERO,
        }
    }

    /// Feeds in an input sample and produces an output level.
    pub fn feed(&mut self, input: f32) -> f32 {
        self.lo_phase += self.center_frequency / self.sample_rate;
        self.lo_phase %= 1.0f32;

        let lo = Complex32::cis(-self.lo_phase * std::f32::consts::TAU);
        let filter_out = self.filter.feed(lo * input);
        let level = (filter_out * self.prev_out.conjugate()).argument()
            / (self.shift * std::f32::consts::TAU / self.sample_rate);
        self.prev_out = filter_out;

        self.symbol_filter.feed(level)
    }

    /// Resets this structure.
    fn reset(&mut self) {
        self.filter.reset();
        self.symbol_filter.reset();
        self.lo_phase = 0.0f32;
        self.prev_out = Complex32::ZERO;
    }

    /// Sets the center frequency.
    pub fn set_center_frequency(&mut self, center_frequency: f32) {
        self.center_frequency = center_frequency;
        self.reset();
    }

    /// Gets the center frequency.
    pub fn get_center_frequency(&self) -> f32 {
        return self.center_frequency;
    }

    /// Gets the frequency shift setting.
    pub fn get_shift(&self) -> f32 {
        return self.shift;
    }
}