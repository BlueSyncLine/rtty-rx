enum DeframerState {
    Idle,
    WaitStop,
    Data,
    SkipSamples(usize)
}

pub enum DeframerOutput {
    None,
    Word(i32),
    StartBit,
    DataBit
}

pub struct SerialDeframer {
    data_bits: usize,
    samples_per_bit: usize,
    invert: bool,
    bit_counter: usize,
    current_word: i32,
    state: DeframerState,
    prev_level: bool,
}

/// A simple deframer for UART-type pulses.
///
/// Synchronizes to start/stop bits and produces words of a given length, LSB-first.
impl SerialDeframer {
    /// Initializes the structure.
    /// `data_bits`: the number of data bits per frame.
    /// `samples_per_bit`: how many level samples are taken for a single bit of data.
    /// `invert`: whether or not invert the bits.
    pub fn new(data_bits: usize, samples_per_bit: usize, invert: bool) -> Self {
        SerialDeframer {
            data_bits,
            samples_per_bit,
            invert,
            bit_counter: 0,
            current_word: 0,
            state: DeframerState::Idle,
            prev_level: true,
        }
    }

    /// Feeds a new level sample into the deframer.
    ///
    /// Returns `DeframerOutput`.
    ///
    /// `None`: no word produced at the current sample.
    /// `SamplePoint`: denotes a sampling instant.
    /// `Word`: an output word.
    pub fn feed(&mut self, level: bool) -> DeframerOutput {
        // Apply inversion if necessary.
        let level = level ^ self.invert;

        let ret = match self.state {
            DeframerState::Idle => {
                // Start bit?
                if !level && self.prev_level {
                    // Skip 1.5 bit durations so that our starting point lands in the center.
                    self.state = DeframerState::SkipSamples(self.samples_per_bit * 3 / 2);

                    // Clear the current word and the bit counter.
                    self.current_word = 0;
                    self.bit_counter = 0;

                    return DeframerOutput::StartBit;
                }
                DeframerOutput::None
            },

            DeframerState::WaitStop => {
                // Stop bit (high)
                if level {
                    self.state = DeframerState::Idle;
                    return DeframerOutput::Word(self.current_word)
                }

                DeframerOutput::None
            },

            DeframerState::SkipSamples(counter) => {
                // More samples left to skip?
                if counter > 1 {
                    self.state = DeframerState::SkipSamples(counter - 1);
                } else {
                    // No, either handle the next data bit or wait for a stop...
                    self.state = if self.bit_counter < self.data_bits {DeframerState::Data} else {DeframerState::WaitStop};
                }

                DeframerOutput::None
            },

            DeframerState::Data => {
                // If not zero, set this bit!
                if level {
                    self.current_word |= 1 << self.bit_counter;
                }

                // Skip to the next bit.
                self.bit_counter += 1;
                self.state = DeframerState::SkipSamples(self.samples_per_bit);

                DeframerOutput::DataBit
            }
        };

        self.prev_level = level;
        return ret;
    }

    /// Resets the deframer.
    pub fn reset(&mut self) {
        self.state = DeframerState::Idle;
    }
}

const ITA2_LTRS: &[u8] = "\0E\nA SIU\rDRJNFCKTZLWHYPQOBG\0MXV\0".as_bytes();
const ITA2_FIGS: &[u8] = "\03\n- \087\r$4',!:(5\")2#6019?&\0./;\0".as_bytes();
const ITA2_SHIFT_LTRS: i32 = 0x1f;
const ITA2_SHIFT_FIGS: i32 = 0x1b;

pub struct ITA2Decoder {
    figs: bool
}

impl ITA2Decoder {
    pub fn new() -> Self {
        Self {
            figs: false
        }
    }

    pub fn feed(&mut self, word: i32) -> Option<u8> {
        assert!(word >= 0 && word <= 31);

        match word {
            ITA2_SHIFT_FIGS => {self.figs = true; None},
            ITA2_SHIFT_LTRS => {self.figs = false; None},
            _ => {
                let c = (if self.figs {ITA2_FIGS} else {ITA2_LTRS})[word as usize];

                // Do not pass through any zero characters.
                if c != 0 {
                    Some(c)
                } else {
                    None
                }
            }
        }
    }

    pub fn reset(&mut self) {
        self.figs = false;
    }
}