use tinycmplx::{Arith, Complex32, ComplexArith};
use crate::util::clamp;

/// Returns a bit-reverse sort of a vector of 2^b elements.
fn bit_reverse_sort<T>(v: &Vec<T>, b: usize) -> Vec<T> where T: Copy {
    // Make sure the length specified is correct.
    assert_eq!(v.len(), 1<<b);

    // Rearrange the elements.
    (0..1<<b).map(|i| {
        // Compute the bit-reversed index.
        let mut a = i;
        let mut j = 0;
        for _ in 0..b {
            j <<= 1;
            j |= a & 1;
            a >>= 1;
        }

        v[j]
    }).collect()
}

/// The Cooley-Tukey FFT algorithm.
/// Computes the FFT of 2^b bins of vector x.
///
/// Refer to: https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm#Data_reordering,_bit_reversal,_and_in-place_algorithms.
pub fn fft(x: &Vec<Complex32>, b: usize) -> Vec<Complex32> {
    // Ensure that the length of x is the expected power of two.
    assert_eq!(x.len(), 1<<b);

    let mut x = bit_reverse_sort(x, b);

    for s in 1..b+1 {
        let m = 1<<s;
        let w_m = Complex32::cis(-std::f32::consts::TAU / (m as f32));

        for k in (0..1<<b).step_by(m) {
            let mut w: Complex32 = Complex32::ONE;

            for j in 0..m/2 {
                let t = w * x[k + j + m/2];
                let u = x[k + j];
                x[k + j] = u + t;
                x[k + j + m/2] = u - t;
                w *= w_m;
            }
        }
    }

    x
}

/// Converts the result of a FFT to a nice, decibel-scale spectrum.
pub fn fft_to_spectrum(x: Vec<Complex32>) -> Vec<f32> {
    // Clamp to prevent infinities from showing up...
    return x[..x.len() / 2].iter().map(|bin| clamp(20f32 * bin.magnitude().log10(), -200f32, 200f32)).collect();
}