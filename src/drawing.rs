use crate::{make_xlat, util};

pub type RGBColor = (f64, f64, f64);

/// Plots a waveform onto a Cairo context.
///
/// `rect` is the rectangle containing the bounding box of the coordinates to be used.
///
/// `range` is the range that the data items lie in.
///
/// `color` is the Cairo color specification.
pub fn plot_waveform(ctx: &gtk::cairo::Context, rect: gtk::Rectangle, data: &Vec<f32>, range: (f64, f64),
                     color: RGBColor) {
    let xlat_x = util::make_xlat((0f64, data.len() as f64), (0f64, rect.width() as f64));

    // Y is mapped upside-down because higher values are located on the top.
    let xlat_y = util::make_xlat(range, (rect.height() as f64, 0f64));

    let points: Vec<(f64, f64)> = data.iter().enumerate().map(|(i, u)| {
        (xlat_x(i as f64), xlat_y(*u as f64))
    }).collect();

    // Fill the area.
    ctx.set_source_rgb(0f64, 0f64, 0f64);
    ctx.rectangle(0f64, 0f64, rect.width() as f64, rect.height() as f64);
    ctx.fill().unwrap();

    // No points, nothing to do.
    if points.len() == 0 {
        return;
    }

    ctx.set_source_rgb(color.0, color.1, color.2);
    ctx.move_to(points[0].0, points[0].1);
    for (x, y) in &points[1..] {
        ctx.line_to(*x, *y);
    }
    ctx.stroke().unwrap();
}

/// Plots a vertical marker on the canvas.
pub fn plot_marker(ctx: &gtk::cairo::Context, rect: gtk::Rectangle, color: RGBColor, value: f32) {
    let xlat = make_xlat((0f64, 1f64), (0f64, rect.width() as f64));
    let x = xlat(value as f64);
    ctx.set_source_rgb(color.0, color.1, color.2);
    ctx.move_to(x, 0f64);
    ctx.line_to(x, rect.height() as f64);
    ctx.stroke().unwrap();
}

/// Plots a horizontal line on the canvas.
pub fn plot_horizontal(ctx: &gtk::cairo::Context, rect: gtk::Rectangle, color: RGBColor) {
    let height = rect.height() as f64;
    ctx.set_source_rgb(color.0, color.1, color.2);
    ctx.move_to(0f64, height / 2f64);
    ctx.line_to(rect.width() as f64, height / 2f64);
    ctx.stroke().unwrap();
}