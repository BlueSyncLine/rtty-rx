use tinycmplx::*;
use crate::sample::Sample;

#[derive(Debug)]
#[derive(Clone)]
pub struct BiquadCoeffs {
    a1: f32,
    a2: f32,
    b0: f32,
    b1: f32,
    b2: f32,
}

impl BiquadCoeffs {
    /// Normalize the given coefficients for a DC gain of one.
    fn normalize_dc(self) -> Self {
        // Equivalent to evaluating the polynomials with z = 1 applied (e^0*tau -> DC).
        let gain = (self.b0 + self.b1 + self.b2) / (1f32 + self.a1 + self.a2);

        Self {
            a1: self.a1,
            a2: self.a2,
            b0: self.b0 / gain,
            b1: self.b1 / gain,
            b2: self.b2 / gain
        }
    }

    /// Low-pass from a conjugate pole pair.
    fn from_conjugate_poles(pole: Complex32) -> Self {
        dbg!(pole);
        Self {
            a1: -pole.real * 2f32,
            a2: pole.magnitude_squared(),

            // Two zeros at Nyquist (z = -1): (x + 1) ^ 2 = x^2 + 2x + 1
            b0: 1f32,
            b1: 2f32,
            b2: 1f32
        }.normalize_dc()
    }
}

#[derive(Debug)]
#[derive(Clone)]
pub struct BiquadStage<T> where T: Sample {
    coeffs: BiquadCoeffs,
    xm1: T,
    xm2: T,
    ym1: T,
    ym2: T
}

impl<T> BiquadStage<T> where T: Sample {
    /// Initializes the biquad filter stage structure.
    pub fn new(coeffs: BiquadCoeffs) -> Self {
        Self {
            coeffs,
            xm1: T::ZERO,
            xm2: T::ZERO,
            ym1: T::ZERO,
            ym2: T::ZERO,
        }
    }

    /// Updates the stage with a new sample and produce an output.
    pub fn feed(&mut self, x: T) -> T {
        let y = x * self.coeffs.b0 + self.xm1 * self.coeffs.b1 + self.xm2 * self.coeffs.b2
            - self.ym1 * self.coeffs.a1 - self.ym2 * self.coeffs.a2;

        // New delayed samples.
        self.xm2 = self.xm1;
        self.xm1 = x;
        self.ym2 = self.ym1;
        self.ym1 = y;

        y
    }

    /// Resets the stage to its initial state.
    pub fn reset(&mut self) {
        self.xm1 = T::ZERO;
        self.xm2 = T::ZERO;
        self.ym1 = T::ZERO;
        self.ym2 = T::ZERO;
    }
}

/// Generate `m` conjugate pairs of Butterworth filter poles (s-domain).
/// Only the first pole of every pair is returned.
fn butterworth_conjugate_poles(m: usize, w0: f32) -> Vec<Complex32> {
    let n= (m * 2) as f32;
    (1..m + 1).map(|k| Complex::cis((2f32 * (k as f32) + n - 1f32) * std::f32::consts::PI / n / 2f32) * w0).collect()
}

/// Bilinear transform, s -> z
fn bilinear(x: Complex32, w0: f32) -> Complex32 {
    let k = w0 / (w0 / 2f32).tan();
    (x + k) / (-x + k)
}

/// A full biquad filter, implemented as a series of stages.
#[derive(Debug)]
#[derive(Clone)]
pub struct BiquadCascade<T> where T: Sample {
    stages: Vec<BiquadStage<T>>,
}

impl<T> BiquadCascade<T> where T: Sample {
    /// Create a new filter cascade structure.
    pub fn new(stage_coeffs: Vec<BiquadCoeffs>) -> Self {
        Self {
            stages: stage_coeffs.iter().map(|coeffs| BiquadStage::<T>::new(coeffs.clone())).collect()
        }
    }

    /// Feed a sample through the cascade.
    pub fn feed(&mut self, x: T) -> T {
        self.stages.iter_mut().fold(x, |x, stage| stage.feed(x))
    }

    /// Reset all stages of the filter structure.
    pub fn reset(&mut self) {
        self.stages.iter_mut().for_each(|stage| stage.reset());
    }

    /// Design a 2m-pole Butterworth lowpass filter.
    pub fn butterworth_lowpass(m: usize, cutoff: f32) -> Self {
        let w0 = cutoff * std::f32::consts::TAU;

        Self::new(butterworth_conjugate_poles(m, w0).iter()
            .map(|pole| BiquadCoeffs::from_conjugate_poles(
                bilinear(*pole, w0))).collect())
    }
}