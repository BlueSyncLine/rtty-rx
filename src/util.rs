use tinycmplx::Complex32;

/// Clamps a numeric value to lie between two bounds.
pub fn clamp<T: PartialOrd>(x: T, min: T, max: T) -> T {
    assert!(min <= max);

    if x < min {
        return min;
    }

    if x > max {
        return max;
    }

    return x;
}

/// Converts a vector of real f32s to a vector of complex values.
pub fn f32_to_cmplx(v: &Vec<f32>) -> Vec<Complex32> {
    v.iter().map(|x| Complex32 {real: *x, imag: 0f32}).collect()
}

/// Update a vector using exponentially-weighted moving average.
pub fn ewma_update(x: &mut Vec<f32>, y: &Vec<f32>, coeff: f32) {
    assert_eq!(x.len(), y.len());
    assert!(coeff >= 0f32 && coeff <= 1f32);

    for i in 0..x.len() {
        x[i] += coeff * (y[i] - x[i]);
    }
}

/// Produces a function to linearly map its argument from the range `original` to `target`.
///
/// Clamping is applied as required.
pub fn make_xlat(original: (f64, f64), target: (f64, f64)) -> Box<dyn Fn(f64) -> f64> {
    let (o_start, o_end) = original;
    let (t_start, t_end) = target;
    let o_delta = o_end - o_start;
    let t_delta = t_end - t_start;

    return Box::new(move |x: f64| {
        t_start + t_delta * (clamp(x, o_start, o_end) - o_start) / o_delta
    })
}

// Picks a binary level based on the sign of a given float.
pub fn binary_slice(x: f32) -> bool {
    return x > 0f32;
}