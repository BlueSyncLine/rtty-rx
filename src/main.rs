use std::sync::*;
use gtk::prelude::*;
use gtk::{Application, ApplicationWindow, Button, DrawingArea, Orientation, ScrolledWindow, TextView};
use cpal::{BufferSize, SampleRate, StreamConfig, StreamError};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use gtk::gdk::EventMask;
use gtk::glib::{idle_add, idle_add_local, SendWeakRef};
use util::make_xlat;
use crate::demod::FSKDemod;
use crate::drawing::{plot_horizontal, plot_marker, plot_waveform};
use crate::tty::{DeframerOutput, ITA2Decoder, SerialDeframer};
use crate::util::{binary_slice, ewma_update, f32_to_cmplx};

mod demod;
mod drawing;
mod iir;
mod util;
mod sample;
mod fft;
mod tty;

const APP_ID: &str = "bluesyncline.rtty";

const SAMPLE_RATE: u32 = 48000;

const FFT_LENGTH_POWER: usize = 13;
const FFT_LENGTH: usize = 1<<FFT_LENGTH_POWER;

// The spectrum is displayed in the 0 - 3 kHz range.
const SPECTRUM_WIDTH: f32 = 3000f32;

const HZ_PER_BIN: f32 = SAMPLE_RATE as f32 / FFT_LENGTH as f32;
const SPECTRUM_BINS: usize = (SPECTRUM_WIDTH / HZ_PER_BIN) as usize;

const AUDIO_CONFIG: StreamConfig = StreamConfig {
    sample_rate: SampleRate(SAMPLE_RATE),
    channels: 1 as _,
    buffer_size: BufferSize::Default
};

const FRAMES_PER_FFT: usize = 3; // 60 / 3 = 20 fps
const FFT_SMOOTHING: f32 = 0.75f32;

const DEMOD_SCOPE_LENGTH: usize = SAMPLE_RATE as usize / 2; // 0.5 seconds are displayed.
const DEMOD_SCOPE_SKIP: usize = DEMOD_SCOPE_LENGTH / 500; // 500 samples displayed per view.

const DEFAULT_CENTER: f32 = 1500.0f32;

#[derive(Copy, Clone)]
enum ScopeMarker {
    None,
    StartBit,
    DataBit
}

struct ApplicationContext {
    demod: Option<FSKDemod>,
    deframer: Option<SerialDeframer>,
    ita2: ITA2Decoder,
    frame_counter: usize,
    fft_index: usize,
    fft_input: Vec<f32>,
    fft_spectrum: Vec<f32>,
    demod_data: Vec<f32>,
    demod_markers: Vec<ScopeMarker>,
    demod_index: usize,

    text_area: SendWeakRef<TextView>,
}

#[derive(Debug)]
struct RTTYParameters {
    baud_rate: f32,
    shift: f32,
    invert: bool
}

impl ApplicationContext {
    /// Applies a set of parameters, recreating the demodulator/deframer structures.
    fn apply_parameters(&mut self, params: &RTTYParameters) {
        eprintln!("Applying {params:?}");
        self.demod = Some(FSKDemod::new(SAMPLE_RATE as f32,
                                   self.demod.as_ref().map_or(DEFAULT_CENTER, |x| x.get_center_frequency()), params.baud_rate, params.shift));
        self.deframer = Some(SerialDeframer::new(5, SAMPLE_RATE as usize / params.baud_rate as usize, params.invert));
    }
}

const PARAMETER_SETS: [(&str, RTTYParameters); 3] = [
    ("RTTY 45.45", RTTYParameters {baud_rate: 500f32/11f32, shift: 170f32, invert: false}),
    ("RTTY 50", RTTYParameters {baud_rate: 50f32, shift: 170f32, invert: false}),
    ("SYNOP", RTTYParameters {baud_rate: 50f32, shift: 450f32, invert: true})
];


fn main() {
    let app = Application::builder().application_id(APP_ID).build();
    let app_ctx = Arc::new(Mutex::new(
        ApplicationContext {
            demod: None,
            deframer: None,
            ita2: ITA2Decoder::new(),
            frame_counter: 0,
            fft_index: 0,
            fft_input: vec![0f32; FFT_LENGTH],
            fft_spectrum: vec![0f32; SPECTRUM_BINS],
            demod_data: vec![0f32; DEMOD_SCOPE_LENGTH],
            demod_markers: vec![ScopeMarker::None; DEMOD_SCOPE_LENGTH],
            demod_index: 0,

            text_area: SendWeakRef::new()
        }
    ));

    let cpal_host = cpal::default_host();
    let cpal_device = cpal_host.default_input_device().expect("Failed to find an input device");

    // A reference to the application context for the processing thread.
    let ctx_mutex = app_ctx.clone();
    let cpal_stream = cpal_device.build_input_stream(
        &AUDIO_CONFIG,
        move |data: &[f32], _| {
            let mut ctx = ctx_mutex.lock().unwrap();

            if ctx.demod.is_none() {
                eprintln!("Attempted to invoke the callback, but haven't initialized yet.");
                return;
            }

            data.iter().for_each(|x| {
                // Feed into the demodulator.
                let demod_out = ctx.demod.as_mut().unwrap().feed(*x);

                let marker = match ctx.deframer.as_mut().unwrap().feed(binary_slice(demod_out)) {
                    DeframerOutput::None => {ScopeMarker::None},
                    DeframerOutput::Word(word) => { ctx.ita2.feed(word).and_then(|char| -> Option<()> {
                        // If not done from the main thread, weird and interesting crashes will be produced.
                        // Ask me how I know!

                        let ctx_mutex_2nd = ctx_mutex.clone();
                        idle_add(move || {
                            let ctx = ctx_mutex_2nd.lock().unwrap();
                            let text_area = ctx.text_area.upgrade().unwrap();
                            let buf = text_area.buffer().unwrap();
                            buf.insert_at_cursor(std::str::from_utf8(&[char]).unwrap());
                            text_area.scroll_to_iter(&mut buf.end_iter(), 0f64, false, 0f64, 0f64);

                            Continue(false)
                        });

                        None
                    }); ScopeMarker::None }
                    DeframerOutput::StartBit => ScopeMarker::StartBit,
                    DeframerOutput::DataBit => ScopeMarker::DataBit
                };

                // Update the FFT samples.
                let fft_index = ctx.fft_index;
                ctx.fft_input[fft_index] = *x;

                ctx.fft_index += 1;
                ctx.fft_index %= FFT_LENGTH;

                // Update the demodulator output samples.
                let demod_index = ctx.demod_index;
                ctx.demod_data[demod_index] = demod_out;
                ctx.demod_markers[demod_index] = marker;
                ctx.demod_index += 1;
                ctx.demod_index %= DEMOD_SCOPE_LENGTH;
            })
        },
        |err: StreamError| eprintln!("Error: {err}")
    ).expect("Failed to create the input stream");

    app.connect_activate(move |app| {
        // Create a drawing area.
        let spectrum_area = DrawingArea::builder().height_request(100).build();

        // Attach an appropriate drawing function.
        let ctx_mutex = app_ctx.clone();
        spectrum_area.connect_draw(move |area, cairo| {
            let rect = area.allocation();
            let ctx = ctx_mutex.lock().unwrap();
            plot_waveform(cairo, rect, &ctx.fft_spectrum, (-30f64, 60f64), (0f64, 1f64, 0f64));

            let demod = ctx.demod.as_ref().unwrap();
            let center_marker = demod.get_center_frequency();
            let lower_marker = center_marker - demod.get_shift() / 2f32;
            let upper_marker = center_marker + demod.get_shift() / 2f32;

            // Draw the frequency markers.
            plot_marker(cairo, rect, (1f64, 0f64, 0f64), center_marker / SPECTRUM_WIDTH as f32);
            plot_marker(cairo, rect, (1f64, 1f64, 0f64), lower_marker / SPECTRUM_WIDTH as f32);
            plot_marker(cairo, rect, (1f64, 1f64, 0f64), upper_marker / SPECTRUM_WIDTH as f32);

            Inhibit(false)
        });

        // Add a callback for every tick.
        let ctx_mutex = app_ctx.clone();
        spectrum_area.add_tick_callback(move |area, _| {
            let mut ctx = ctx_mutex.lock().unwrap();

            // Increment the frame counter...
            ctx.frame_counter += 1;

            // Is it time to do an FFT?
            if ctx.frame_counter == FRAMES_PER_FFT {
                ctx.frame_counter = 0;

                // Perform an FFT. Convert to spectrum bins and truncate.
                let spectrum = fft::fft_to_spectrum(
                    fft::fft(&f32_to_cmplx(&ctx.fft_input), FFT_LENGTH_POWER)
                )[..SPECTRUM_BINS].to_vec();

                // Update using exponentially-weighted moving average.
                ewma_update(&mut ctx.fft_spectrum, &spectrum, FFT_SMOOTHING);
            }

            area.queue_draw();
            Continue(true)
        });

        // Attach a mouse button press event.
        let ctx_mutex = app_ctx.clone();
        spectrum_area.set_events(EventMask::BUTTON_PRESS_MASK);
        spectrum_area.connect_button_press_event(move |area, event| {
            let mut ctx = ctx_mutex.lock().unwrap();
            let (x, _) = event.position();
            let xlat = make_xlat((0f64, area.allocation().width() as f64), (0f64, SPECTRUM_WIDTH as f64));
            let new_center = xlat(x) as f32;
            ctx.demod.as_mut().unwrap().set_center_frequency(new_center);

            eprintln!("New center frequency set: {new_center}");

            Inhibit(false)
        });

        // Create a drawing area for a scope view of the demodulated signal.
        let demod_area = DrawingArea::builder().height_request(100).build();
        let ctx_mutex = app_ctx.clone();
        demod_area.connect_draw(move |area, cairo| {
            let rect = area.allocation();
            let ctx = ctx_mutex.lock().unwrap();

            // For performance, reduce the length of data actually displayed vastly.
            let decimated_data = (0..DEMOD_SCOPE_LENGTH).step_by(DEMOD_SCOPE_SKIP).map(
                |i| ctx.demod_data[i]
            ).collect();
            plot_waveform(cairo, rect, &decimated_data, (-2f64, 2f64), (0f64, 1f64, 0f64));
            plot_horizontal(cairo, rect, (0f64, 0.5f64, 0.5f64));

            // Place markers on bit sampling points.
            for (i, marker) in ctx.demod_markers.iter().enumerate() {
                let x = i as f32 / DEMOD_SCOPE_LENGTH as f32;
                match marker {
                    ScopeMarker::None => {},
                    ScopeMarker::StartBit => plot_marker(cairo, rect, (0f64, 1f64, 1f64), x),
                    ScopeMarker::DataBit => plot_marker(cairo, rect, (1f64, 0f64, 0f64), x)
                }
            }
            Inhibit(false)
        });

        // Redraw every tick.
        demod_area.add_tick_callback(|area, _| {
            area.queue_draw();
            Continue(true)
        });

        // Create a box layout.
        let layout = gtk::Box::builder().orientation(Orientation::Vertical).build();
        layout.pack_start(&spectrum_area, true, true, 0);
        layout.pack_start(&demod_area, true, true, 0);

        // Create a combo box for the different RTTY modes.
        let combo_baudshift = gtk::ComboBoxText::builder().build();
        PARAMETER_SETS.iter().for_each(|(k, _)| combo_baudshift.append_text(k));

        let ctx_mutex = app_ctx.clone();
        combo_baudshift.connect_changed(move |combo| {
            eprintln!("Selection changed!");
            let mut ctx = ctx_mutex.lock().unwrap();
            let active_index = combo.active().unwrap() as usize;
            ctx.apply_parameters(&PARAMETER_SETS[active_index].1);
        });
        combo_baudshift.set_active(Some(0));

        // A button to clear everything.
        let clear_button = Button::builder().label("Clear").build();
        let ctx_mutex = app_ctx.clone();
        clear_button.connect_clicked(move |button| {
            let ctx = ctx_mutex.lock().unwrap();
            let text = ctx.text_area.upgrade().unwrap();
            let buf = text.buffer().unwrap();
            buf.delete(&mut buf.start_iter(), &mut buf.end_iter());
        });

        let controls_layout = gtk::Box::builder().orientation(Orientation::Horizontal).build();
        controls_layout.pack_start(&combo_baudshift, false, false, 0);
        controls_layout.pack_start(&clear_button, false, false, 0);
        layout.pack_start(&controls_layout, false, false, 0);

        // Create a scrolled section for the text area.
        let scrolled = ScrolledWindow::builder().height_request(300).build();

        // Create a text area for output.
        let text_area = TextView::builder().build();

        // Prevent the user from moving the cursor.
        text_area.set_events(EventMask::BUTTON_PRESS_MASK);
        text_area.connect_button_press_event(|area, evt| {
            Inhibit(true)
        });

        // Add to the scrolled window.
        scrolled.add(&text_area);

        layout.pack_start(&scrolled, false, false, 0);
        app_ctx.lock().unwrap().text_area.set(Some(&text_area));

        let window = ApplicationWindow::builder().application(app)
            .title("RTTY").child(&layout).width_request(800).build();

        window.show_all();
        cpal_stream.play().expect("Failed to start the input stream");
    });

    app.run();
}
