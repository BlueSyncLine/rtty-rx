use std::ops::{Add, Sub, Div, Mul};
use tinycmplx::{Arith, Complex32};

pub trait Sample: Arith + Add<f32, Output = Self> + Sub<f32, Output = Self> + Mul<f32, Output = Self> + Div<f32, Output = Self> {}
impl Sample for f32 {}
impl Sample for Complex32 {}